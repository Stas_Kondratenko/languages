<?php

namespace app\models;
use yii\db\ActiveRecord;

class Location extends ActiveRecord 
{
    private $location_id;
    private $parent_id;
    private $name;
    
    public static function tableName(){
        return 'location';
    }
}  
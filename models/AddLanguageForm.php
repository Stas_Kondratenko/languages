<?php
namespace app\models;

use yii\base\Model;

class AddLanguageForm extends Model
{
    public $location_id;
    public $language_id;
    public $name;
    
    public function rules()  {
           return [  
            ['name', 'filter', 'filter' => 'trim'],
            ['name', 'required'],
            ['name', 'string','min' => 3, 'max' => 20],
               
            ['language_id', 'filter', 'filter' => 'trim'],
            ['language_id', 'integer', 'max' => 100000],
            
            ['location_id', 'filter', 'filter' => 'trim'],
            ['location_id', 'integer', 'max' => 100000],         
        ];
    }
}
<?php
namespace app\models;

use yii\base\Model;

class AddLocationForm extends Model
{
    public $id;
    public $parent_id;
    public $name;
    
    public function rules()  {
           return [  
            ['name', 'filter', 'filter' => 'trim'],
            ['name', 'required'],
            ['name', 'string','min' => 3, 'max' => 20],
               
            ['parent_id', 'filter', 'filter' => 'trim'],
            ['parent_id', 'integer', 'max' => 100000],
               
            ['id', 'filter', 'filter' => 'trim'],
            ['id', 'integer', 'max' => 100000],
        ];
    }
}
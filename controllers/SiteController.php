<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use app\models\Location;
use yii\data\ActiveDataProvider;
use app\models\Language;
use app\models\AddLocationForm;
use yii\web\Response;
use yii\widgets\ActiveForm;
use yii\data\SqlDataProvider;
use app\models\AddLanguageForm;
use app\models\Location_language;

class SiteController extends Controller{
    
    public function actions(){
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionIndex(){
        $countries = Location::find()->where(['parent_id'=> null])->all();
        $cities = Location::find()->andWhere(['>','parent_id',0])->all();
        $languages = Language::find()->all();
        $languageDataProvider =  new ActiveDataProvider([
            'query' => Language::find(),
            'pagination' => ['pageSize' => 5]]); 
        $countryDataProvider =  new ActiveDataProvider([
            'query' => Location::find()->where(['parent_id'=> null]),
            'pagination' => ['pageSize' => 7]]); 
        $cityDataProvider =  new ActiveDataProvider([
            'query' => Location::find()->andWhere(['>','parent_id',0]),
            'pagination' => ['pageSize' => 7]]); 
        $model = new AddLocationForm();  
        if ($model->load(Yii::$app->request->post())){
            $cityDataProvider =  new ActiveDataProvider([
            'query' => Location::find()->where(['parent_id'=>$model->parent_id]),
            'pagination' => ['pageSize' => 7]]);  
            $cities = Location::find()->andWhere(['parent_id'=>$model->parent_id])->all();
            $_SESSION['selected_country'] = $model->parent_id;
        }
        return $this->render('index', ['countries' => $countries,
            'cities' => $cities,
            'model' => new Location(),
            'languageDataProvider' => $languageDataProvider,
            'countryDataProvider' =>$countryDataProvider,
            'cityDataProvider' =>$cityDataProvider,
            'locationModel'=> new AddLocationForm(),
            'parent_id'=> $model->parent_id,
            'language_model'=> new AddLanguageForm(),
            'languages'=>$languages]);
    }

    public function actionAbout() {
        return $this->render('about');
    }
    
    public function actionLanguage(){
        $model = new AddLocationForm(); 
        if ($model->load(Yii::$app->request->post())){
            $countries = Location::find()->where(['parent_id'=> null])->all();
            $cities = Location::find()->andWhere(['>','parent_id',0])->all();
            $languages = Language::find()->all();
            $count = Yii::$app->db->createCommand('
                    SELECT COUNT(*) FROM location_language WHERE location_id = :location_id',
                   [':location_id' => $model->id])->queryScalar();
            $languageDataProvider = new SqlDataProvider(['sql' => 
                'SELECT language.language_id, language.name
                 FROM language INNER JOIN location_language
                 ON location_language.language_id=language.language_id
                 WHERE location_language.location_id = :location_id',
                 'params' => [':location_id' => $model->id],
                 'totalCount' => $count,
                 'pagination' => [
                              'pageSize' => 5,
                 ],
                 'sort' => [
                        'attributes' => [
                                     'language_id',
                                     'name' => [
                                        'asc' => ['first_name' => SORT_ASC, 'last_name' => SORT_ASC],
                                        'desc' => ['first_name' => SORT_DESC, 'last_name' => SORT_DESC],
                                        'default' => SORT_DESC,
                                        'label' => 'Name',
                                         ],
                         ]
                 ]
                ]);
            $countryDataProvider =  new ActiveDataProvider([
                'query' => Location::find()->where(['parent_id'=> null]),
                'pagination' => ['pageSize' => 7]]); 
            $cityDataProvider =  new ActiveDataProvider([
                'query' => Location::find()->andWhere(['>','parent_id',0]),
                'pagination' => ['pageSize' => 7]]); 
            $_SESSION['selected_city'] = $model->id;
            setcookie('selected_city',$model->id,time()+1800,"/");
            setcookie('selected_country',$model->parent_id,time()+1800,"/");
            $_SESSION['selected_country'] = $model->parent_id;
            return $this->render('index', ['countries' => $countries,
                'cities' => $cities,
                'model' => new Location(),
                'languageDataProvider' => $languageDataProvider,
                'countryDataProvider' =>$countryDataProvider,
                'cityDataProvider' =>$cityDataProvider,
                'locationModel'=> new AddLocationForm(),
                'parent_id'=> $model->parent_id,
                'language_model'=> new AddLanguageForm(),
                'languages'=>$languages ]);   
        }
    }
        
    public function actionAddlocation(){
       $model = new AddLocationForm();  
       if ($model->load(Yii::$app->request->post())){ 
           if($model->validate()){
                $country = new Location();
                $country->name = $model->name;
                if($model->parent_id!=null){
                    $country->parent_id = $model->parent_id;            
                }
                try{
                     $country->save();
                     return $this->actionIndex();
                }
                catch(yii\db\Exception $ex){echo "Произошла ошибка!";}            
           }
           else {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validate($model); 
            }              
       } 
       else{return $this->actionIndex();}
    }
    
    public function actionAddexistinglanguage(){
        $model = new AddLanguageForm();
        if ($model->load(Yii::$app->request->post())){ 
                if($this->SaveRelation($model)){
                    return $this->ReturnState();
                }
                else{echo "That language is existing in that city. Check, please!";} 
            }
       }        
    
    public function SaveRelation($language){
        $relation = new Location_language();
        $relation->location_id = $language->location_id;
        $relation->language_id = $language->language_id;
        if($relation->save()){return true;}
        else{ return false;}
    }
         
    public function actionAddnewlanguage(){
        $model = new AddLanguageForm();
        if ($model->load(Yii::$app->request->post())){ 
            if($model->validate()){
                $language = new Language();
                $language->name = $model->name;
                try{
                    $language->save();
                    $languageId = Language::find()->where(['name'=> $model->name])->one();
                    $model->language_id = $languageId['language_id'];
                    if($this->SaveRelation($model)){
                        return $this->ReturnState();
                    }
                }catch(yii\db\Exception $ex){echo "That language is existing yet. Check again please!";}  
            }
            else {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validate($model); 
            }          
        }        
    }
   
    public function actionDeletelocation($id){
        Location::deleteAll(['location_id' => $id]);
        Location::deleteAll(['parent_id' => $id]);
        return $this->actionIndex();
    }
    
    public function actionDeletelanguage($id){
       $locationId = isset($_COOKIE['selected_city']) ? $_COOKIE['selected_city'] : 0;
       Location_language::deleteAll(['language_id' => $id,'location_id' => $locationId]);
       return $this->ReturnState();
    }
    
    public function actionDeleteforever($id){
       $locationId = isset($_COOKIE['selected_city']) ? $_COOKIE['selected_city'] : 0;
       Location_language::deleteAll(['language_id' => $id,'location_id' => $locationId]);
       Language::deleteAll(['language_id' => $id]);
      return $this->actionIndex(); 
    }
    
    public function ReturnState(){
            $cityId = isset($_COOKIE['selected_city']) ? $_COOKIE['selected_city'] : 0;
            $countryId = isset($_COOKIE['selected_country']) ? $_COOKIE['selected_country'] : 0;
            
            $countries = Location::find()->where(['parent_id'=> null])->all();
            $cities = Location::find()->andWhere(['>','parent_id',0])->all();
            $languages = Language::find()->all();
            if($cityId == 0){
                $languageDataProvider =  new ActiveDataProvider([
                    'query' => Language::find(),
                    'pagination' => ['pageSize' => 5]]);  
            }
            else{
                $count = Yii::$app->db->createCommand('
                    SELECT COUNT(*) FROM location_language WHERE location_id = :location_id',
                   [':location_id' => $cityId])->queryScalar();
                $languageDataProvider = new SqlDataProvider(['sql' => 
                'SELECT language.language_id, language.name
                 FROM language INNER JOIN location_language
                 ON location_language.language_id=language.language_id
                 WHERE location_language.location_id = :location_id',
                 'params' => [':location_id' => $cityId],
                 'totalCount' => $count,
                 'pagination' => [
                              'pageSize' => 5,
                 ],
                 'sort' => [
                        'attributes' => [
                                     'language_id',
                                     'name' => [
                                        'asc' => ['first_name' => SORT_ASC, 'last_name' => SORT_ASC],
                                        'desc' => ['first_name' => SORT_DESC, 'last_name' => SORT_DESC],
                                        'default' => SORT_DESC,
                                        'label' => 'Name',
                                         ],
                         ]
                 ]
                ]);
                
            }
            
            $countryDataProvider =  new ActiveDataProvider([
                'query' => Location::find()->where(['parent_id'=> null]),
                'pagination' => ['pageSize' => 7],
                 ]); 
            $cityDataProvider =  new ActiveDataProvider([
                'query' => Location::find()->andWhere(['>','parent_id',0]),
                'pagination' => ['pageSize' => 7],
                 ]); 
            $_SESSION['selected_city'] = $cityId;
            setcookie('selected_city',$cityId,time()+200,"/");
            setcookie('selected_country',$countryId,time()+200,"/");
            $_SESSION['selected_country'] = $countryId;
            return $this->render('index', ['countries' => $countries,
                'cities' => $cities,
                'model' => new Location(),
                'languageDataProvider' => $languageDataProvider,
                'countryDataProvider' =>$countryDataProvider,
                'cityDataProvider' =>$cityDataProvider,
                'locationModel'=> new AddLocationForm(),
                'parent_id'=> $countryId,
                'language_model'=> new AddLanguageForm(),
                'languages'=>$languages ]);   
        }         
}



<?php
use yii\widgets\Pjax;
use yii\grid\GridView;
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
?>

<div class="modal fade" id="edit-city-window">
            <div class="modal-dialog">
              <div class="modal-content">
                  <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                      <h4 class="modal-title">Edit cities</h4>
                  </div>
                  <div class="modal-body">
                                            
                 <?php Pjax::begin(['id' => 'edit-city-container','timeout' => 5000]); ?>
                 <?php $cityForm = ActiveForm::begin([
                                 'id' => 'city-form',
                                 'action' => ['site/index'],
                                 'options' => ['onsubmit' => 'return false','class' => 'form-inline','data-pjax' => true],
                                 'enableClientValidation' => true 
                                  ]); ?>
                      
                      <div class="form-group">
                            <div class="col-xs-4">
                              <?= $cityForm->field($locationModel,'parent_id')->dropDownList(
                                ArrayHelper::map($countries,'location_id', 'name'),
                                ['prompt' => "Select country",
                                 'options' => ['class' => 'dropdown-list form-control'],
                                  'id' => 'add-city-country-field' ])->label(false) ?>
                             </div>   
                           <?php ActiveForm::end()?> 
                     </div>
                            <?= GridView::widget([
                                'dataProvider' => $cityDataProvider,
                                'summary'=>false,
                                'columns' => [
                                    'location_id',
                                    'name',
                                    [
                                        'class' => 'yii\grid\ActionColumn',
                                        'template' =>  '{delete}',
                                        'buttons' =>[
                                            'delete' => function ($url,$model) {
                                            return Html::a(
                                                    '<span class="glyphicon glyphicon-trash"></span>', 
                                                    ['site/deletelocation', 'id' => $model->location_id, 'data-pjax'=> true]);
                                                 },  
                                        ]
                                        
                                    ],
                                ],
                            ]); ?>
                      <?php $form = ActiveForm::begin([
                                 'id' => 'add-city-form',
                                 'action' => ['site/addlocation'],
                                 'options' => ['onsubmit' => 'return false','class' => 'form-inline','data-pjax' => true],
                                 'enableClientValidation' => true 
                                  ]); ?>
                        
                                 <?php echo $form->field($locationModel, 'name')->textInput(['options'=>['class'=>'form-control', 'id' => 'city-input']])->label("New city name") ?>
                                 <?= $form->field($locationModel, 'parent_id')->hiddenInput(['value'=>$parent_id])->label(false); ?>
                          <?php ActiveForm::end()?>
                          <?php Pjax::end(); ?> 
                     
                  </div>
                  <div class="modal-footer"> 
                      <button type="submit" class="btn btn-primary" id="add-city-btn">Add city</button>
                      <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                  </div>
                  
              </div>
           </div>
 </div> 
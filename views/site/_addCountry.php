<?php
use yii\widgets\Pjax;
use yii\grid\GridView;
use yii\widgets\ActiveForm;
use yii\helpers\Html;
?>

<div class="modal fade" id="edit-country-window">
            <div class="modal-dialog">
              <div class="modal-content">
                  <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                      <h4 class="modal-title">Edit countries</h4>
                  </div>
                  <div class="modal-body">
                     <?php Pjax::begin(['id' => 'edit-location-container','timeout' => 5000]); ?>
                            <?= GridView::widget([
                                'dataProvider' => $countryDataProvider,
                                'summary'=>false,
                                'columns' => [
                                    'location_id',
                                    'name',
                                    [
                                        'class' => 'yii\grid\ActionColumn',
                                        'template' =>  '{delete}',
                                        'buttons' =>[
                                            'delete' => function ($url,$model) {
                                            return Html::a(
                                                    '<span class="glyphicon glyphicon-trash"></span>', 
                                                    ['site/deletelocation', 'id' => $model->location_id, 'data-pjax'=> true]);
                                                 },  
                                        ]
                                        
                                    ],
                                ],
                            ]); ?>
                    
                       
                      <?php $form = ActiveForm::begin([
                                 'id' => 'country-form',
                                 'action' => ['site/addlocation'],
                                 'options' => ['onsubmit' => 'return false','class' => 'form-inline','data-pjax' => true],
                                 'enableClientValidation' => true 
                                  ]); ?>
                      <?= $form->field($locationModel, 'name')->textInput(['options'=>['class'=>'form-control', 'id' => 'country-input']])->label('New country name') ?>
                      <?php ActiveForm::end()?>
                       <?php Pjax::end(); ?> 
 
                  </div>
                  <div class="modal-footer"> 
                       <button type="submit" class="btn btn-primary" id="add-country-btn">Add country</button>  
                       <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                  </div>
                  
              </div>
           </div>
 </div> 
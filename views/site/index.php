<?php
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\Html;
$this->title = 'Languages';

?>

<?php echo $this->render("_addCountry",['countryDataProvider'=> $countryDataProvider,'locationModel' => $locationModel]); ?>
<?php echo $this->render("_addCity",['cityDataProvider'=> $cityDataProvider,'locationModel' => $locationModel,'countries' => $countries, 'parent_id'=>$parent_id]); ?>
<div class="container">
    <div class="site-index">
                 <?php Pjax::begin(['id' => 'main-form', 'timeout' => 10000]);?>
                 <?php $countryForm = ActiveForm::begin([
                                 'id' => 'select-country-form',
                                 'action' => ['site/index'],
                                 'options' => ['onsubmit' => 'return false','data-pjax' => true]
                                  ]); ?>

                 <div class="form-group">
                        <div class="col-xs-offset-3 col-xs-4">
                         <?php $selected_country  = isset($_SESSION['selected_country']) ? $_SESSION['selected_country'] : 0 ?>
                          <?= $countryForm->field($locationModel,'parent_id')->dropDownList(
                                            ArrayHelper::map($countries,'location_id', 'name'),
                                           ['prompt' => "Select country",
                                           'options' => ['class' => 'dropdown-list'],
                                           'id' => 'country_field',
                                           'options' => [$selected_country => ['selected ' => true]]])->label(false) ?>                      
                         </div>
                    <div class="col-xs-2">
                        <button type="button" class="btn btn-primary edit-btn" id="edit-country-btn">Edit</button>
                    </div>
                </div>
               <?php ActiveForm::end(); ?>
                <div class="form-group">
                     <div class="col-xs-offset-3 col-xs-4">
                            <?php $cityForm = ActiveForm::begin([
                                            'id' => 'select-city-form',
                                            'action' => ['site/language'],
                                            'options' => ['onsubmit' => 'return false','data-pjax' => true]
                                             ]); ?>
                             <?php $selected_city  = isset($_SESSION['selected_city']) ? $_SESSION['selected_city'] : 0 ?>
                             <?= $cityForm->field($locationModel,'id')->dropDownList(
                                            ArrayHelper::map($cities,'location_id', 'name'),
                                            ['prompt' => "Select city",
                                            'id' => 'city_field',
                                            'options' => [$selected_city => ['selected ' => true]]])->label(false) ?>
                             <?= $cityForm->field($locationModel, 'parent_id')->hiddenInput(['value'=>$selected_country])->label(false); ?>
                             <?php ActiveForm::end();?>
                         
                      </div>
                      <div class="col-xs-2">
                            <button type="button" class="btn btn-primary edit-btn" id="edit-city-btn">Edit</button>
                      </div>
                </div>
                          
        
        <div class="form-group">
            <div class="col-xs-offset-3 col-xs-5">
                <?php 
                echo GridView::widget([
                'dataProvider' => $languageDataProvider,
                'tableOptions' => ['class' => 'table table-hover languages table-condensed table-striped table-bordered' ],  
                'summary'=>false,
                'columns' => [
                    [
                    'label' => 'Id',
                    'attribute' => 'language_id',
                    'value'=>function($data){
                    return $data["language_id"];}
                    ],
                    
                    [
                    'label' => 'Languages',
                    'attribute' => 'name',
                    'value'=>function($data){
                    return $data["name"];}
                    ],
                    [
                     'class' => 'yii\grid\ActionColumn',
                      'template' =>  '{delete}{deleteforever}',
                      'buttons' =>[
                                  'delete' => function ($url,$data) {
                                   if(!isset($_SESSION['selected_city'])){
                                   return Html::a('<span class="glyphicon glyphicon-trash"></span>', 
                                   ['site/deleteforever', 'id' => $data['language_id'], 'data-pjax'=> true]);}
                                   }, 
                                           
                                  'deleteforever'=> function ($url,$data) {
                                    if(isset($_SESSION['selected_city'])){
                                        return Html::a('<span class="glyphicon glyphicon-remove"></span>', 
                                       ['site/deletelanguage', 'id' => $data['language_id'], 'data-pjax'=> true]);}
                                    },      
                                                         ]    
                                    ],
                ]]);
                ?> 
            </div>
        </div>
                <div class="form-group<?php if($selected_city == 0){echo ' hidden';}?>">
                     <div class="col-xs-offset-3 col-xs-4">
                    <?php $newLanguageform = ActiveForm::begin([
                                     'id' => 'add-new-language-form',
                                     'action' => ['site/addnewlanguage'],
                                     'options' => ['onsubmit' => 'return false','data-pjax' => true],
                                     'enableClientValidation' => true]); ?>
                          <?= $newLanguageform->field($language_model, 'name')->textInput(['options'=>['class'=>'form-control hidden'],'placeholder'=>'Add new language'])->label(false) ?>
                          <?= $newLanguageform->field($language_model, 'location_id')->hiddenInput(['value'=>$selected_city])->label(false); ?>   
                     </div>
                    <div class="col-xs-2">  
                        <button type="submit" class="btn btn-primary" id="add-new-language-form-submit">Add</button>
                    </div>
                  <?php ActiveForm::end(); ?> 
                </div>
                
              
                 <div class="form-group <?php if($selected_city == 0){echo ' hidden';}?>">
                     <div class="col-xs-offset-3 col-xs-4">
                     <?php $languageForm = ActiveForm::begin([
                                 'id' => 'select-existing-language-form',
                                 'action' => ['site/addexistinglanguage'],
                                 'options' => ['onsubmit' => 'return false','data-pjax' => true]
                                  ]); ?>

                 
                        
                          <?= $languageForm->field($language_model,'language_id')->dropDownList(
                                            ArrayHelper::map($languages,'language_id', 'name'),
                                           ['prompt' => "Select existing language",
                                           'options' => ['class' => 'dropdown-list'],
                                           'enableClientValidation' => false ])->label(false) ?> 
                         <?= $languageForm->field($language_model, 'location_id')->hiddenInput(['value'=>$selected_city])->label(false); ?>
                         </div>
                    <div class="col-xs-2">
                        <button type="submit" class="btn btn-primary edit-btn"id="select-existing-language-submit">Add</button>
                    </div>
                </div>
                <?php ActiveForm::end()?>

             <?php Pjax::end(); ?> 
        </div>
    </div>



jQuery(document).ready(function ($) {
  
    $("#add-country-btn").click(function () {
        $("#country-form").submit();  
     });
    
    $("#add-city-btn").click(function () {
        $("#add-city-form").submit();  
    });
    
    $(document).on("click",'#select-existing-language-submit',function(){ $("#select-existing-language-form").submit();});
    
    $(document).on("click",'#add-new-language-form-submit',function(){ $("#add-new-language-form").submit();});
    
    $(document).on("change",'#country_field',function(){ $("#select-country-form").submit();});
    
    $(document).on("change",'#add-city-country-field',function(){ $("#city-form").submit(); });
    
    $(document).on("change",'#city_field',function(){ $("#select-city-form").submit();});
   
    $(document).on("click",'#edit-country-btn',function(){ $('#edit-country-window').modal();});   
 
    $(document).on("click",'#edit-city-btn',function(){ $('#edit-city-window').modal();});  
    
    $('#edit-country-window').on('hidden.bs.modal', function () {
       window.location.reload(true);
     });
     
    $('#edit-city-window').on('hidden.bs.modal', function () {
       window.location.reload(true);
     });
          
 });
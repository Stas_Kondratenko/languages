<?php
namespace app\assets;

use yii\web\AssetBundle;

class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'assets/content/css/main.css'
    ];
    public $js = [
        'assets/content/js/main.js'
    ];
   
}
